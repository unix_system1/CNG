ARG BUILD_IMAGE=

FROM ${BUILD_IMAGE}

ARG VERSION=master
ARG NAMESPACE=gitlab-org
ARG PROJECT=gitaly
ARG API_URL=
ARG API_TOKEN=
ARG FIPS_MODE=0

ARG BUNDLE_OPTIONS="--jobs 4"

ADD gitlab-ruby.tar.gz /
ADD gitlab-go.tar.gz /

ENV LANG=C.UTF-8
ENV PRIVATE_TOKEN=${API_TOKEN}
ENV LIBDIR ${LIBDIR:-"/usr/lib64"}

COPY shared/build-scripts/ /build-scripts

RUN dnf ${DNF_OPTS} install -by --nodocs git \
    && dnf clean all \
    && rm -r /var/cache/dnf

RUN mkdir /assets \
    && ln -sf /usr/local/go/bin/* /usr/local/bin \
    && /gitlab-fetch \
        "${API_URL}" \
        "${NAMESPACE}" \
        "${PROJECT}" \
        "${VERSION}" \
    && cd ${PROJECT}-${VERSION}/ruby \
    && bundle config set --local without 'development test' \
    && bundle install ${BUNDLE_OPTIONS} \
    && cd .. \
    && cp -R ./ruby /srv/gitaly-ruby \
    && install -D LICENSE /licenses/GitLab.txt \
    && rm -rf /srv/gitaly-ruby/spec /srv/gitaly-ruby/features \
    && touch .ruby-bundle \
    && FIPS_MODE=${FIPS_MODE} make install WITH_BUNDLED_GIT=YesPlease \
    && /build-scripts/cleanup-gems ${LIBDIR}/ruby/gems \
    && cp -R --parents \
      /usr/local/bin/gitaly* \
      /usr/local/bin/praefect \
      ${LIBDIR}/ruby/gems/ \
      /srv/gitaly-ruby \
      /licenses \
      /assets
